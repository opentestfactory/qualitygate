# Copyright (c) 2021-2024 Henix, Henix.fr
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Some quality gate unit tests."""


import json
import logging
import requests
import sys
import unittest
from unittest.mock import MagicMock, patch, mock_open
from uuid import UUID


sys.argv = ['dummy', '--trusted-authorities', '/dev/null']

from opentf.qualitygate import qualitygate

########################################################################
# Datasets

QUALITYGATE = {
    'quaLItygAte': [
        {
            'name': 'allTests',
            'rule': {
                'scope': "(test.technology == 'cypress')",
                'threshold': '100%',
                'failure-status': ['failure'],
            },
        }
    ]
}

STRICT_QUALITYGATE = {
    'strict': [
        {
            'name': 'Default strict quality gate',
            'rule': {
                'scope': 'true',
                'threshold': '100%',
                'failure-status': ['failure'],
            },
        }
    ]
}

PASSING_QUALITYGATE = {
    'passing': [
        {
            'name': 'Default passing quality gate',
            'rule': {
                'scope': 'true',
                'threshold': '0%',
                'failure-status': ['failure'],
            },
        }
    ]
}

QUALITYGATES_DICT = {
    'qualitygates': [
        {
            'name': 'tesTqG',
            'rules': [
                {
                    'name': 'nOtAllTesTs',
                    'rule': {
                        'scope': '(test.technology == "rRroBot")',
                        'threshold': '100%',
                        'failure-status': ['failure', 'error'],
                    },
                }
            ],
        }
    ]
}

QUALITYGATES_WITH_DUPLICATES = {
    'qualitygates': [
        {
            'name': 'DoUblE',
            'rules': [
                {
                    'name': 'rule3',
                    'rule': {
                        'scope': '(test.name == "nAmE")',
                        'threshold': '0%',
                    },
                }
            ],
        },
        {
            'name': 'DoUblE',
            'rules': [
                {
                    'name': 'rule3',
                    'rule': {
                        'scope': '(test.technology == "rRroBot")',
                        'threshold': '100%',
                    },
                }
            ],
        },
        {
            'name': 'triPle',
            'rules': [
                {
                    'name': 'rule1',
                    'rule': {
                        'scope': '(test.technology == "JUNit")',
                        'threshold': '100%',
                    },
                },
                {
                    'name': 'rule1',
                    'rule': {
                        'scope': '(test.technology == "tEcHnO")',
                        'threshold': '10%',
                    },
                },
            ],
        },
    ]
}

TC_METADATA = [
    {
        'name': 'nAmE',
        'status': 'SUCCESS',
        'test': {
            'technology': 'cypress',
        },
        'uuid': 'caseId',
    }
]


UUID_OK = 'bb2e5737-1aee-4fad-97e8-2d46c1984b3e'

########################################################################
# Helpers


def build_observer_payload_mock(
    observer_status="DONE", tests_status=None, namespace='default'
):
    if tests_status is None:
        tests_status = {}
    status_objects = [
        {
            'metadata': {'namespace': namespace},
            'test': {'outcome': status, 'technology': 'cypress'},
            'status': status,
            'uuid': test_id,
            'name': 'nAmE',
        }
        for test_id, status in tests_status.items()
    ]
    return {
        "details": {
            "status": observer_status,
            "items": status_objects,
        }
    }


def build_observer_mock(http_status=200, observer_status="DONE", tests_status=None):
    response = requests.Response()
    response.status_code = http_status
    response.json = MagicMock(
        return_value=build_observer_payload_mock(observer_status, tests_status)
    )
    return response


########################################################################
# Tests


class QualityGateTests(unittest.TestCase):
    def setUp(self):
        logging.disable(logging.CRITICAL)
        app = qualitygate.app
        app.config['CONTEXT']['services'] = {
            'observer': {'endpoint': 'http://localhost'}
        }
        app.config['CONTEXT']['enable_insecure_login'] = True
        app.config['CONFIG']['qualitygates'] = {
            'passing': {},
            'strict': {},
            'foobar': {},
            'barfoo': {},
            'bar': {},
            'foo': {},
        }
        self.app = app.test_client()
        self.assertEqual(app.debug, False)

    def tearDown(self):
        logging.disable(logging.NOTSET)

    # observer response codes

    def test_respond_422_when_mode_is_unknown(self):
        response = self.app.get(f'/workflows/{UUID_OK}/qualitygate?mode=invalid')
        self.assertEqual(response.status_code, 422)

    def test_respond_422_when_observer_said_422(self):
        mockreponse = requests.Response()
        mockreponse.status_code = 422
        with patch('requests.get', MagicMock(return_value=mockreponse)):
            response = self.app.get(f'/workflows/{UUID_OK}/qualitygate?mode=bar')
        self.assertEqual(response.status_code, 422)

    def test_respond_500_when_observer_said_418(self):
        mockreponse = requests.Response()
        mockreponse.status_code = 418
        with patch('requests.get', MagicMock(return_value=mockreponse)), patch(
            'opentf.qualitygate.qualitygate.error'
        ) as mock_error:
            response = self.app.get(f'/workflows/{UUID_OK}/qualitygate?mode=foobar')
        self.assertEqual(response.status_code, 500)
        mock_error.assert_called_once_with('Unexpected observer response: %d.', 418)

    def test_respond_404_when_observer_said_404(self):
        mockresponse = requests.Response()
        mockresponse.status_code = 404
        with patch('requests.get', MagicMock(return_value=mockresponse)):
            response = self.app.get(f'/workflows/{UUID_OK}/qualitygate?mode=foo')
            self.assertEqual(response.status_code, 404)

    def test_respond_401_when_observer_said_401(self):
        mockresponse = requests.Response()
        mockresponse.status_code = 401
        with patch('requests.get', MagicMock(return_value=mockresponse)):
            response = self.app.get(f'/workflows/{UUID_OK}/qualitygate?mode=foobar')
            self.assertEqual(response.status_code, 401)

    def test_respond_403_when_observer_said_403(self):
        mockresponse = requests.Response()
        mockresponse.status_code = 403
        with patch('requests.get', MagicMock(return_value=mockresponse)):
            response = self.app.get(f'/workflows/{UUID_OK}/qualitygate?mode=bar')
            self.assertEqual(response.status_code, 403)

    def test_respond_200_when_observer_said_200(self):
        mockresponse = requests.Response()
        mockresponse.status_code = 200
        mockresponse.json = lambda: {
            'details': {
                'status': 'DONE',
                'items': [{'metadata': {'namespace': 'foo'}}],
            }
        }
        mock_handle = MagicMock()
        with patch('requests.get', MagicMock(return_value=mockresponse)), patch(
            'opentf.qualitygate.qualitygate.can_use_namespace',
            MagicMock(return_value=True),
        ), patch(
            'opentf.qualitygate.qualitygate.STATUS_HANDLER', {'DONE': mock_handle}
        ):
            response = self.app.get(f'/workflows/{UUID_OK}/qualitygate?mode=barfoo')
            mock_handle.assert_called_once()
            self.assertEqual(response.status_code, 200)

    def test_respond_403_when_observer_said_200(self):
        mockresponse = requests.Response()
        mockresponse.status_code = 200
        mockresponse.json = lambda: {
            'details': {
                'status': 'DONE',
                'items': [{'metadata': {'namespace': 'foo'}}],
            }
        }
        with patch('requests.get', MagicMock(return_value=mockresponse)), patch(
            'opentf.qualitygate.qualitygate.can_use_namespace',
            MagicMock(return_value=False),
        ):
            response = self.app.get(f'/workflows/{UUID_OK}/qualitygate?mode=strict')
            self.assertEqual(response.status_code, 403)

    # quality gate response codes for workflow statuses

    def test_respond_FAILURE_when_workflow_is_invalid(self):
        app = qualitygate.app
        with app.app_context(), patch(
            'opentf.qualitygate.qualitygate.can_use_namespace',
            lambda n, **_: n == 'default',
        ), patch(
            'opentf.qualitygate.qualitygate._get_namespace',
            MagicMock(return_value='default'),
        ):
            response = build_observer_mock(
                200, observer_status="UNEXPECTED", tests_status={'dummytest': 'FAILURE'}
            )
            quality_gate_response = qualitygate.process_known_workflow(
                'woRfkloW', response, "strict", {'foo': 'bar'}
            )
            self.assertEqual(422, quality_gate_response.status_code)

    def test_respond_FAILURE_when_workflow_is_failure(self):
        app = qualitygate.app
        with app.app_context(), patch(
            'opentf.qualitygate.qualitygate.can_use_namespace',
            lambda n, **_: n == 'default',
        ), patch(
            'opentf.qualitygate.qualitygate._get_namespace',
            MagicMock(return_value='default'),
        ):
            response = build_observer_mock(
                200, observer_status="FAILED", tests_status={'dummytest': 'SUCCESS'}
            )
            quality_gate_response = qualitygate.process_known_workflow(
                'woRfkloW', response, "strict", {'foo': 'bar'}
            )
            response_json = quality_gate_response.json
            quality_gate_status = response_json["details"]["status"]
            self.assertEqual(200, quality_gate_response.status_code)
            self.assertEqual("FAILURE", quality_gate_status)

    def test_respond_RUNNING_when_workflow_is_pending(self):
        app = qualitygate.app
        with app.app_context(), patch(
            'opentf.qualitygate.qualitygate.can_use_namespace',
            lambda n, **_: n == 'default',
        ):
            response = build_observer_mock(200, observer_status="PENDING")
            quality_gate_response = qualitygate.process_known_workflow(
                'woRfkloW', response, "strict", {'foo': 'bar'}
            )
            response_json = quality_gate_response.json
            quality_gate_status = response_json["details"]["status"]
            self.assertEqual(200, quality_gate_response.status_code)
            self.assertEqual("RUNNING", quality_gate_status)

    def test_respond_RUNNING_when_workflow_is_running(self):
        app = qualitygate.app
        with app.app_context(), patch(
            'opentf.qualitygate.qualitygate.can_use_namespace',
            lambda n, **_: n == 'default',
        ):
            response = build_observer_mock(200, observer_status="RUNNING")
            quality_gate_response = qualitygate.process_known_workflow(
                'woRfkloW', response, "strict", {'foo': 'bar'}
            )
            response_json = quality_gate_response.json
            quality_gate_status = response_json["details"]["status"]
            self.assertEqual(200, quality_gate_response.status_code)
            self.assertEqual("RUNNING", quality_gate_status)

    def test_respond_NOTEST_when_no_test_results(self):
        app = qualitygate.app
        with app.app_context(), patch(
            'opentf.qualitygate.qualitygate.can_use_namespace',
            lambda n, **_: n == 'default',
        ), patch(
            'opentf.qualitygate.qualitygate._get_namespace',
            MagicMock(return_value='default'),
        ):
            response = build_observer_mock(200)
            quality_gate_response = qualitygate.process_known_workflow(
                'woRfkloW', response, "strict", {'foo': 'bar'}
            )
            response_json = quality_gate_response.json
            quality_gate_status = response_json["details"]["status"]
            self.assertEqual(200, quality_gate_response.status_code)
            self.assertEqual("NOTEST", quality_gate_status)

    def test_respond_FORBIDDEN_with_passing_strategy_nons(self):
        app = qualitygate.app
        with app.app_context(), patch(
            'opentf.qualitygate.qualitygate.can_use_namespace',
            lambda n, **_: False,
        ):
            response = build_observer_mock(
                200, tests_status={"id1": "SUCCESS", "id2": "FAILURE"}
            )
            quality_gate_response = qualitygate.process_known_workflow(
                'woRfkloW', response, "passing", {'foo': 'bar'}
            )
            self.assertEqual(403, quality_gate_response.status_code)

    # get namespace

    def test_get_namespace_empty(self):
        self.assertRaises(ValueError, qualitygate._get_namespace, [])

    def test_get_namespace_nons(self):
        self.assertRaises(
            ValueError, qualitygate._get_namespace, [{}, {'metadata': {'name': 'foo'}}]
        )

    def test_get_namespace_ok(self):
        self.assertEqual(
            qualitygate._get_namespace([{'metadata': {'namespace': 'foo'}}]), 'foo'
        )

    # evaluate quality gate

    def test_evaluate_quality_gate_ok(self):
        tc_metadata = [
            {
                'name': 'cAsEName',
                'status': 'SUCCESS',
                'test': {
                    'technology': 'cypress',
                },
                'uuid': 'caSeId',
            }
        ]

        qg_result = qualitygate.evaluate_quality_gate(
            'quaLItygAte', tc_metadata, QUALITYGATE
        )
        self.assertEqual('SUCCESS', qg_result[0]['allTests']['result'])

    def test_evaluate_quality_gate_ko(self):
        tc_metadata = [
            {
                'name': 'cAsEName',
                'status': 'FAILURE',
                'test': {
                    'technology': 'cypress',
                },
                'uuid': 'caSeId',
            }
        ]
        qg_result = qualitygate.evaluate_quality_gate(
            'quaLItygAte', tc_metadata, QUALITYGATE
        )
        self.assertEqual('FAILURE', qg_result[0]['allTests']['result'])

    def test_evaluate_quality_gate_notest(self):
        tc_metadata = [
            {
                'name': 'cAsE',
                'status': 'SUCCESS',
                'test': {'technology': 'junit'},
                'uuid': 'caSeId',
            }
        ]
        qg_result = qualitygate.evaluate_quality_gate(
            'quaLItygAte', tc_metadata, QUALITYGATE
        )
        self.assertEqual('NOTEST', qg_result[0]['allTests']['result'])

    def test_evaluate_quality_gate_mode_ko(self):
        with self.assertRaises(Exception) as ex:
            qualitygate.evaluate_quality_gate('nOmOde', [], QUALITYGATE)
            self.assertEqual('Quality gate nOmOde not defined.', str(ex.exception))

    def test_evaluate_quality_gate_treshold_ko(self):
        qg_ko_treshold = {
            'tresholdKo': [
                {
                    'name': 'tTests',
                    'rule': {
                        'scope': "(test.technology == 'cypress')",
                        'threshold': 'one hundred and one percent',
                        'failure-status': ['failure'],
                    },
                }
            ]
        }
        tc_metadata = [
            {
                'name': 'cAsEName',
                'status': 'FAILURE',
                'test': {
                    'technology': 'cypress',
                },
                'uuid': 'caSeId',
            }
        ]
        with self.assertRaises(Exception) as ex:
            qualitygate.evaluate_quality_gate('tresholdKo', tc_metadata, qg_ko_treshold)
        self.assertIn(
            'Qualitygate threshold one hundred and one percent is invalid.',
            str(ex.exception),
        )

    def test_evaluate_quality_gate_scope_datakey_ko(self):
        tc_metadata = [
            {
                'name': 'cAsEName',
                'status': 'FAILURE',
                'test': {'name': 'tesT', 'data': {'DSNAME': 'Dataset not Cypress'}},
                'uuid': 'caSeId',
            }
        ]
        qg_nodata = {
            'quaLItygAte': [
                {
                    'name': 'noTests',
                    'rule': {
                        'scope': "(test.data.DSNAME == 'Dataset Cypress')",
                        'threshold': '100%',
                        'failure-status': ['failure'],
                    },
                }
            ]
        }
        qg_result = qualitygate.evaluate_quality_gate(
            'quaLItygAte', tc_metadata, qg_nodata
        )
        self.assertEqual('NOTEST', qg_result[0]['noTests']['result'])

    def test_evaluate_quality_gate_scope_throws_value_error(self):
        qg_invalid_conditional = {
            'quaLItygAte': [
                {
                    'name': 'noTests',
                    'rule': {
                        'scope': 'foo bar',
                        'threshold': '100%',
                        'failure-status': ['failure'],
                    },
                }
            ]
        }
        qg_result = qualitygate.evaluate_quality_gate(
            'quaLItygAte', TC_METADATA, qg_invalid_conditional
        )
        self.assertEqual('INVALID_SCOPE', qg_result[0]['noTests']['result'])
        self.assertIn('Invalid conditional foo bar', qg_result[0]['noTests']['message'])

    def test_evaluate_quality_gate_scope_throws_key_error(self):
        tc_metadata = [{'test': {}, 'uuid': 'caSeId'}]
        qg_invalid_property = {
            'quaLItygAte': [
                {
                    'name': 'noTests',
                    'rule': {
                        'scope': "test.use.path=='path'",
                        'threshold': '100%',
                        'failure-status': ['failure'],
                    },
                }
            ]
        }
        qg_result = qualitygate.evaluate_quality_gate(
            'quaLItygAte', tc_metadata, qg_invalid_property
        )
        self.assertEqual('INVALID_SCOPE', qg_result[0]['noTests']['result'])
        self.assertIn(
            'Nonexisting context entry in expression test.use.path',
            qg_result[0]['noTests']['message'],
        )

    def test_evaluate_quality_gate_rule_without_name(self):
        tc_metadata = [
            {
                'name': 'cAsEName',
                'status': 'SUCCESS',
                'test': {
                    'name': 'tesT',
                },
                'uuid': 'caSeId',
            }
        ]
        qg_norulename = {
            'quaLItygAte': [
                {
                    'rule': {
                        'scope': "(test.technology == 'cypress')",
                        'threshold': '100%',
                        'failure-status': ['failure'],
                    },
                }
            ]
        }
        qg_result = qualitygate.evaluate_quality_gate(
            'quaLItygAte', tc_metadata, qg_norulename
        )
        keys = [key for key in qg_result[0].keys()]
        self.assertEqual(1, len(keys))
        self.assertTrue(UUID(keys[0]))

    # handle completed workflow

    def test_handle_completed_workflow_ok(self):
        app = qualitygate.app
        req = MagicMock()
        req.args = {}
        eqg_mock = MagicMock(return_value=({'rUle': {'result': 'SUCCESS'}}, None))
        with app.app_context(), patch(
            'opentf.qualitygate.qualitygate.can_use_namespace',
            lambda n, **_: n == 'default',
        ), patch(
            'opentf.qualitygate.qualitygate.evaluate_quality_gate',
            eqg_mock,
        ), patch(
            'opentf.qualitygate.qualitygate.request', req
        ), patch(
            'opentf.qualitygate.qualitygate.annotate_response'
        ):
            response = build_observer_mock(200, tests_status={'caSeId': 'SUCCESS'})
            qualitygate.handle_completed_workflow(
                'woRfkloW', response, 'strict', {'foo': 'bar'}
            )
            eqg_mock.assert_called_once()
            metadata = eqg_mock.call_args[0][1][0]
            self.assertEqual('nAmE', metadata['name'])
            self.assertEqual('cypress', metadata['test']['technology'])

    def test_handle_completed_workflow_ok_paginated(self):
        resp1 = MagicMock()
        resp1.json.return_value = {'details': {'items': [{'dummytest': 'failure'}]}}
        resp1.links = {'next': {'url': 'foobar'}}
        resp1.request.headers = {}
        resp2 = MagicMock()
        resp2.json.return_value = {'details': {'items': [{'dummytest2': 'success'}]}}
        resp2.links = {}
        resp2.request.headers = {}
        mock_get = MagicMock(side_effect=resp2)
        mock_eqg = MagicMock(side_effect=Exception('Quit'))
        with patch('opentf.qualitygate.qualitygate.requests.get', mock_get), patch(
            'opentf.qualitygate.qualitygate.evaluate_quality_gate', mock_eqg
        ):
            self.assertRaisesRegex(
                Exception,
                'Quit',
                qualitygate.handle_completed_workflow,
                'wf',
                resp1,
                'strict',
                {},
            )
        mock_get.assert_called_once()
        mock_eqg.assert_called_once()

    def test_handle_completed_workflow_ko_exception(self):
        resp1 = MagicMock()
        resp1.json.return_value = {'details': {'items': [{'dummytest': 'failure'}]}}
        resp1.links = {'next': {'url': 'foobar'}}
        resp1.request.headers = {}
        mock_get = MagicMock(side_effect=ValueError('Boo'))
        with patch('opentf.qualitygate.qualitygate.requests.get', mock_get), patch(
            'opentf.qualitygate.qualitygate.error'
        ) as mock_error, patch(
            'opentf.qualitygate.qualitygate.make_status_response'
        ) as mock_msr:
            qualitygate.handle_completed_workflow('wf', resp1, 'passing', {})
        mock_get.assert_called_once()
        mock_error.assert_called_once_with(
            'Could not deserialize observer response: %s.', 'Boo'
        )
        mock_msr.assert_called_once_with(
            'Invalid', 'Error while querying datasources: Boo.'
        )

    def test_handle_completed_workflow_with_duplicate_warnings(self):
        app = qualitygate.app
        req = MagicMock()
        req.args = {}
        qg_rule_double = {
            'triPle': QUALITYGATES_WITH_DUPLICATES['qualitygates'][2]['rules']
        }
        msr_mock = MagicMock()
        with app.app_context(), patch(
            'opentf.qualitygate.qualitygate.can_use_namespace',
            lambda n, **_: n == 'default',
        ), patch(
            'opentf.qualitygate.qualitygate.make_status_response', msr_mock
        ), patch(
            'opentf.qualitygate.qualitygate.request', req
        ), patch(
            'opentf.qualitygate.qualitygate.annotate_response'
        ):
            response = build_observer_mock(200, tests_status={'caSeId': 'SUCCESS'})
            qualitygate.handle_completed_workflow(
                'woRfkloW', response, 'triPle', qg_rule_double, ['foo']
            )
            msr_mock.assert_called_once()
            warnings = msr_mock.call_args[0][2]['warnings']
            self.assertEqual('foo', warnings[0])
            self.assertIn('Duplicate rule name(s) found: rule1.', warnings[1])

    def test_handle_completed_workflow_general_result_success(self):
        app = qualitygate.app
        req = MagicMock()
        req.args = {}
        qg_rule_success = {
            'rUlE': QUALITYGATES_WITH_DUPLICATES['qualitygates'][0]['rules']
        }
        msr_mock = MagicMock()
        with app.app_context(), patch(
            'opentf.qualitygate.qualitygate.can_use_namespace',
            lambda n, **_: n == 'default',
        ), patch(
            'opentf.qualitygate.qualitygate.make_status_response', msr_mock
        ), patch(
            'opentf.qualitygate.qualitygate.in_scope', MagicMock(return_value=True)
        ), patch(
            'opentf.qualitygate.qualitygate.request', req
        ), patch(
            'opentf.qualitygate.qualitygate.annotate_response'
        ):
            response = build_observer_mock(200, tests_status={'caSeId': 'SUCCESS'})
            qualitygate.handle_completed_workflow(
                'woRfkloW', response, 'rUlE', qg_rule_success, None
            )
            msr_mock.assert_called_once()
            self.assertTrue(msr_mock.call_args[0][2]['rules'].get('rule3'))
            self.assertEqual('SUCCESS', msr_mock.call_args[0][2]['status'])

    # default quality gates

    def test_respond_SUCCESS_with_passing_strategy(self):
        app = qualitygate.app
        req = MagicMock()
        req.args = {}
        mock_ar = lambda x, **_: x
        with app.app_context(), patch(
            'opentf.qualitygate.qualitygate.can_use_namespace',
            lambda n, **_: n == 'default',
        ), patch('opentf.qualitygate.qualitygate.request', req), patch(
            'opentf.qualitygate.qualitygate.annotate_response', mock_ar
        ):
            qualitygate._load_default_qg_definitions()
            response = build_observer_mock(
                200, tests_status={"id1": "SUCCESS", "id2": "FAILURE"}
            )
            quality_gate_response = qualitygate.handle_completed_workflow(
                'woRfkloW', response, 'passing', PASSING_QUALITYGATE
            )
            response_json = quality_gate_response.json
            quality_gate_status = response_json["details"]  # type: ignore
            self.assertEqual(200, quality_gate_response.status_code)
            self.assertEqual(
                'SUCCESS',
                quality_gate_status['rules']['Default passing quality gate']['result'],
            )

    def test_respond_FAILURE_with_strict_strategy_and_a_test_failure(self):
        app = qualitygate.app
        req = MagicMock()
        req.args = {}
        mock_ar = lambda x, **_: x
        with app.app_context(), patch(
            'opentf.qualitygate.qualitygate.can_use_namespace',
            lambda n, **_: n == 'default',
        ), patch('opentf.qualitygate.qualitygate.request', req), patch(
            'opentf.qualitygate.qualitygate.annotate_response', mock_ar
        ):
            response = build_observer_mock(
                200, tests_status={"id1": "FAILURE", "id2": "SUCCESS", "id3": "SUCCESS"}
            )
            quality_gate_response = qualitygate.process_known_workflow(
                'woRfkloW', response, "strict", STRICT_QUALITYGATE
            )
            response_json = quality_gate_response.json
            quality_gate_status = response_json["details"]  # type: ignore
            self.assertEqual(200, quality_gate_response.status_code)
            self.assertEqual(
                'FAILURE',
                quality_gate_status['rules']['Default strict quality gate']['result'],
            )

    def test_respond_FAILURE_with_strict_strategy_and_a_test_error(self):
        app = qualitygate.app
        req = MagicMock()
        req.args = {}
        mock_ar = lambda x, **_: x
        mock_testcases = MagicMock(
            return_value={
                'id1': {'status': 'FAILURE'},
                'id2': {'status': 'SUCCESS'},
                'id3': {'status': 'SUCCESS'},
            }
        )
        with app.app_context(), patch(
            'opentf.qualitygate.qualitygate.can_use_namespace',
            lambda n, **_: n == 'default',
        ), patch('opentf.qualitygate.qualitygate.request', req), patch(
            'opentf.qualitygate.qualitygate.annotate_response', mock_ar
        ):
            response = build_observer_mock(
                200, tests_status={"id1": "FAILURE", "id2": "SUCCESS", "id3": "SUCCESS"}
            )
            quality_gate_response = qualitygate.process_known_workflow(
                'woRfkloW', response, 'strict', STRICT_QUALITYGATE
            )
            response_json = quality_gate_response.json
            quality_gate_status = response_json["details"]  # type: ignore
            self.assertEqual(200, quality_gate_response.status_code)
            self.assertEqual(
                'FAILURE',
                quality_gate_status['rules']['Default strict quality gate']['result'],
            )

    def test_respond_SUCCESS_with_strict_strategy_and_a_test_success(self):
        app = qualitygate.app
        req = MagicMock()
        req.args = {}
        mock_ar = lambda x, **_: x
        with app.app_context(), patch(
            'opentf.qualitygate.qualitygate.can_use_namespace',
            lambda n, **_: n == 'default',
        ), patch('opentf.qualitygate.qualitygate.request', req), patch(
            'opentf.qualitygate.qualitygate.annotate_response', mock_ar
        ):
            response = build_observer_mock(
                200, tests_status={"id1": "SUCCESS", "id2": "SUCCESS", "id3": "SUCCESS"}
            )
            quality_gate_response = qualitygate.process_known_workflow(
                'woRfkloW', response, 'strict', STRICT_QUALITYGATE
            )
            response_json = quality_gate_response.json
            quality_gate_status = response_json["details"]  # type: ignore
            self.assertEqual(200, quality_gate_response.status_code)
            self.assertEqual(
                'SUCCESS',
                quality_gate_status['rules']['Default strict quality gate']['result'],
            )

    # read quality gate definition

    def test_read_quality_gate_definition_ok(self):
        app = qualitygate.app
        mock_read = mock_open(read_data=json.dumps(QUALITYGATES_DICT))

        with app.app_context(), patch('builtins.open', mock_read), patch(
            'opentf.qualitygate.qualitygate.validate_schema',
            MagicMock(return_value=(True, 'bar')),
        ):
            qualitygate._read_qg_definition(app, 'foo', 'bar')
            mock_read.assert_called_once()
            qg = app.config['CONFIG'].get('qualitygates')
            self.assertIn('tesTqG', qg.keys())
            self.assertIn('strict', qg.keys())
            self.assertIn('passing', qg.keys())
            self.assertEqual('nOtAllTesTs', qg['tesTqG'][0]['name'])

        del app.config['CONFIG']['qualitygates']

    def test_read_new_quality_gate_definition(self):
        app = qualitygate.app
        mock_read = mock_open(read_data=json.dumps(QUALITYGATES_DICT))
        mock_logger = MagicMock()
        mock_logger.info = MagicMock()
        app.logger = mock_logger

        if app.config['CONFIG'].get('qualitygates'):
            del app.config['CONFIG']['qualitygates']

        with app.app_context(), patch('builtins.open', mock_read), patch(
            'opentf.qualitygate.qualitygate.validate_schema',
            MagicMock(return_value=(True, 'bar')),
        ):
            qualitygate._read_qg_definition(app, 'foo', 'bar')
            mock_read.assert_called_once()
            self.assertEqual(
                'Reading quality gate definition from foo.',
                mock_logger.info.call_args_list[0][0][0],
            )

    def test_read_quality_gate_definition_ko(self):
        app = qualitygate.app
        mock_read = mock_open(read_data=None)
        mock_logger = MagicMock()
        mock_logger.info = MagicMock()
        mock_logger.error = MagicMock()
        app.logger = mock_logger

        with app.app_context(), patch('builtins.open', mock_read), patch(
            'opentf.qualitygate.qualitygate.validate_schema',
            MagicMock(return_value=(True, 'bar')),
        ):
            qualitygate._read_qg_definition(app, 'foo', 'bar')
            qg = app.config['CONFIG'].get('qualitygates')
            mock_read.assert_called_once()
            mock_logger.info.assert_called_once()
            mock_logger.error.assert_called_once()
            self.assertIn(
                'Loading default quality gate definitions',
                mock_logger.info.call_args[0][0],
            )
            self.assertIn(
                'Invalid quality gate definition file foo',
                mock_logger.error.call_args[0][0],
            )
            self.assertIn('strict', qg.keys())
            self.assertIn('passing', qg.keys())

    def test_read_quality_gate_definition_missing_file(self):
        mock_read = mock_open(read_data='foo')
        with patch('builtins.open', mock_read):
            qualitygate._read_qg_definition('miam', None, 'baRr')
        mock_read.assert_not_called()

    def test_read_quality_gate_definition_invalid_file(self):
        app = qualitygate.app
        mock_read = mock_open(read_data=json.dumps(QUALITYGATES_DICT))
        mock_logger = MagicMock()
        mock_logger.error = MagicMock()
        app.logger = mock_logger

        with app.app_context(), patch('builtins.open', mock_read), patch(
            'opentf.qualitygate.qualitygate.validate_schema',
            MagicMock(return_value=(False, 'meow')),
        ):
            qualitygate._read_qg_definition(app, 'foo', 'bar')
        mock_read.assert_called_once()
        mock_logger.error.assert_called_once_with(
            'Error while verifying quality gate definition foo: meow.'
        )

    def test_read_quality_gate_definition_ko_with_exception(self):
        app = qualitygate.app
        mock_read = mock_open(read_data='dAtA')
        mock_read.side_effect = FileNotFoundError('File not found')
        mock_logger = MagicMock()
        mock_logger.error = MagicMock()
        app.logger = mock_logger
        vs_mock = MagicMock()

        with app.app_context(), patch('builtins.open', mock_read), patch(
            'opentf.qualitygate.qualitygate.validate_schema', vs_mock
        ):
            qualitygate._read_qg_definition(app, 'foo', 'bar')
        mock_read.assert_called_once()
        vs_mock.assert_not_called()
        mock_logger.error.assert_called_once_with(
            'Error while reading foo quality gate definition: File not found.'
        )

    def test_read_quality_gate_definition_with_duplicates(self):
        app = qualitygate.app
        mock_read = mock_open(read_data=json.dumps(QUALITYGATES_WITH_DUPLICATES))
        mock_warning = MagicMock()
        msg = 'Duplicate quality gate name(s) found: DoUblE'

        with app.app_context(), patch('builtins.open', mock_read), patch(
            'opentf.qualitygate.qualitygate.validate_schema',
            MagicMock(return_value=(True, 'bar')),
        ), patch('opentf.qualitygate.qualitygate.warning', mock_warning):
            qualitygate._read_qg_definition(app, 'foo', 'bar')
            mock_read.assert_called_once()
            mock_warning.assert_called_once()
            self.assertIn(msg, mock_warning.call_args[0][0])
            self.assertIn(msg, app.config['CONFIG'].get('qualitygate_warnings')[0])

    # start qualitygate app

    def test_start_qualitygate_with_definition_file(self):
        mock_wf = MagicMock()
        mock_ra = MagicMock()
        mock_svc = MagicMock()
        mock_svc.config = {'CONTEXT': {'services': {'observer': 'SOMETHING'}}}

        with patch('opentf.qualitygate.qualitygate.watch_file', mock_wf), patch.dict(
            'os.environ', {'QUALITYGATE_DEFINITIONS': '/high/way/to'}
        ), patch('opentf.qualitygate.qualitygate.run_app', mock_ra):
            qualitygate.main(mock_svc)
        mock_wf.assert_called_once()
        self.assertEqual('/high/way/to', mock_wf.call_args[0][1])
        mock_ra.assert_called_once_with(mock_svc)

    def test_start_qualitygate_without_definition_file(self):
        app = qualitygate.app
        mock_rqd = MagicMock()
        mock_ra = MagicMock()
        mock_svc = MagicMock()

        with app.app_context(), patch(
            'opentf.qualitygate.qualitygate._read_qg_definition', mock_rqd
        ), patch('opentf.qualitygate.qualitygate.run_app', mock_ra):
            qualitygate.main(mock_svc)
        mock_rqd.assert_called_once()
        self.assertEqual(None, mock_rqd.call_args[0][1])
        mock_ra.assert_called_once_with(mock_svc)

    # handle quality gate definition

    def test_handle_qualitygate_definition_json_ok(self):
        json_data = QUALITYGATES_DICT
        meq_mock = MagicMock(return_value='oK')
        with patch(
            'opentf.qualitygate.qualitygate._maybe_evaluate_qualitygate', meq_mock
        ):
            response = self.app.post(
                '/workflows/wF_Id/qualitygate',
                data=json.dumps(json_data),
                headers={'Content-Type': 'application/json'},
            )
        meq_mock.assert_called_once()
        self.assertEqual('wF_Id', meq_mock.call_args[0][0])
        self.assertIn('tesTqG', meq_mock.call_args[0][1])
        self.assertIn('oK', response.text)

    def test_handle_qualitygate_definition_json_ko(self):
        json_data = '1+2+3'
        meq_mock = MagicMock()
        with patch(
            'opentf.qualitygate.qualitygate._maybe_evaluate_qualitygate', meq_mock
        ):
            response = self.app.post(
                '/workflows/wF_Id/qualitygate',
                data=json_data,
                headers={'Content-Type': 'application/json'},
            )
        self.assertEqual(
            'Could not parse quality gate definition from file.',
            response.json['message'],
        )
        meq_mock.assert_not_called()

    def test_handle_qualitygate_definition_yaml_ok(self):
        yaml_data = QUALITYGATES_DICT
        meq_mock = MagicMock(return_value='oKy')
        with patch(
            'opentf.qualitygate.qualitygate._maybe_evaluate_qualitygate', meq_mock
        ):
            response = self.app.post(
                '/workflows/wF_Id/qualitygate',
                data=json.dumps(yaml_data),
                headers={'Content-Type': 'application/x-yaml'},
            )
        meq_mock.assert_called_once()
        self.assertEqual('wF_Id', meq_mock.call_args[0][0])
        self.assertIn('tesTqG', meq_mock.call_args[0][1])
        self.assertIn('oKy', response.text)

    def test_handle_qualitygate_definition_yaml_ko(self):
        meq_mock = MagicMock()
        with patch(
            'opentf.qualitygate.qualitygate._maybe_evaluate_qualitygate', meq_mock
        ):
            response = self.app.post(
                '/workflows/wF_Id/qualitygate',
                data='',
                headers={'Content-Type': 'application/x-yaml'},
            )
        meq_mock.assert_not_called()
        self.assertIn('YAML quality gate definitions', response.json['message'])

    def test_handle_qualitygate_definition_file_ok(self):
        meq_mock = MagicMock(return_value='oKf')
        with patch(
            'opentf.qualitygate.qualitygate._maybe_evaluate_qualitygate', meq_mock
        ):
            response = self.app.post(
                '/workflows/wF_Id/qualitygate',
                data={
                    'qualitygates': open(
                        'tests/python/resources/qualitygate.yaml', 'rb'
                    )
                },
            )
        meq_mock.assert_called_once()
        self.assertEqual('wF_Id', meq_mock.call_args[0][0])
        self.assertIn('my.quality.gate', meq_mock.call_args[0][1])
        self.assertIn('oKf', response.text)

    def test_handle_qualitygate_definition_file_not_found(self):
        meq_mock = MagicMock(return_value='oKf')
        with patch(
            'opentf.qualitygate.qualitygate._maybe_evaluate_qualitygate', meq_mock
        ):
            response = self.app.post(
                '/workflows/wF_Id/qualitygate',
                data={'quality': open('tests/python/resources/qualitygate.yaml', 'rb')},
            )
            self.assertEqual(
                'Could not parse quality gate definition from file.',
                response.json['message'],
            )

    def test_handle_qualitygate_definition_invalid_schema(self):
        meq_mock = MagicMock(return_value='noTOk')
        vs_mock = MagicMock(return_value=(False, 'nope'))
        with patch(
            'opentf.qualitygate.qualitygate._maybe_evaluate_qualitygate', meq_mock
        ), patch('opentf.qualitygate.qualitygate.validate_schema', vs_mock):
            response = self.app.post(
                '/workflows/wF_Id/qualitygate',
                data={
                    'qualitygates': open(
                        'tests/python/resources/qualitygate.yaml', 'rb'
                    )
                },
            )
        meq_mock.assert_not_called()
        self.assertEqual(
            'Not a valid quality gate definition: nope', response.json['message']
        )

    def test_handle_qualitygate_definition_invalid_workflow_id(self):
        vs_mock = MagicMock(return_value=(True, 'yep'))
        with patch('opentf.qualitygate.qualitygate.validate_schema', vs_mock):
            response = self.app.post(
                '/workflows/faLse_wF_Id/qualitygate?mode=my.quality.gate',
                data={
                    'qualitygates': open(
                        'tests/python/resources/qualitygate.yaml', 'rb'
                    )
                },
            )
        self.assertEqual(
            'This parameter is not a valid UUID: faLse_wF_Id', response.json['message']
        )
        self.assertEqual(422, response.json['code'])

    def test_handle_qualitygate_definition_with_name_duplicates(self):
        json_data = QUALITYGATES_WITH_DUPLICATES
        meq_mock = MagicMock(return_value='oK')
        with patch(
            'opentf.qualitygate.qualitygate._maybe_evaluate_qualitygate', meq_mock
        ):
            self.app.post(
                '/workflows/wF_Id/qualitygate',
                data=json.dumps(json_data),
                headers={'Content-Type': 'application/json'},
            )
        meq_mock.assert_called_once()
        self.assertIn(
            'Duplicate quality gate name(s) found: DoUblE', meq_mock.call_args[0][2][0]
        )


if __name__ == "__main__":
    unittest.main()
